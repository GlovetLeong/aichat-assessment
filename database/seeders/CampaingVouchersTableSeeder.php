<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Helpers\General;


class CampaingVouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaign_voucher_list = [];
        for ($i=0; $i < 10 ; $i++) { 
            $campaign_voucher_list[] = [
                'campaign_id' => 1,
                'name' => 'Voucher ' . ($i),
                'code' => General::generateRandomString(10),
                'discount_type' => config('constants.discount_type.flat'),
                'discount_amount' => 10,
                'status' => config('constants.status.active'),
                'start_date' => date('Y-m-d 00:00:00'),
                'end_date' => date('Y-m-d 23:59:59'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }

        DB::table('campaign_vouchers')->insert($campaign_voucher_list);
    }
}