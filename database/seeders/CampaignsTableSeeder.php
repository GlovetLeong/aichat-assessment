<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaigns')->insert([
            [
                'id' => 1,
                'company_id' => 1,
                'name' => 'Anniversary Celebration Loyal',
                'description' => 'A Sample Seed Campaign',
                'status' => config('constants.status.active'),
                'start_date' => date('Y-m-d 00:00:00'),
                'end_date' => date('Y-m-d 23:59:59'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}