<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->call(CustomersTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);

        $this->call(CampaignsTableSeeder::class);

        $this->call(CampaingVouchersTableSeeder::class);
        
        $this->call(CustomerPurchaseTransactionsTableSeeder::class);



        DB::commit();
    }
}
