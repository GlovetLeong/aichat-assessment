<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CustomerPurchaseTransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_purchase_transactions')->insert([
            [
                'id' => 1,
                'customer_id' => 1,
                'total_spent' => 10,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'customer_id' => 1,
                'total_spent' => 20,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'customer_id' => 1,
                'total_spent' => 30,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'customer_id' => 1,
                'total_spent' => 40,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 5,
                'customer_id' => 1,
                'total_spent' => 50,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 6,
                'customer_id' => 2,
                'total_spent' => 50,
                'total_saving' => 0,
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}