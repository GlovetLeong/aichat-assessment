<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRedeemCampaignProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_redeem_campaign_processes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('campaign_id')->unsigned()->nullable();
            $table->foreign('campaign_id', 'crcp_ca_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id', 'crcps_cu_id')->references('id')->on('customers')->onDelete('cascade');
            $table->bigInteger('campaign_voucher_id')->unsigned()->nullable();
            $table->foreign('campaign_voucher_id', 'crcp_cv_id')->references('id')->on('campaign_vouchers')->onDelete('cascade');
            $table->json('img')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->timestamps();

            $table->index(['status', 'created_at'], 'crcp_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_redeem_campaign_processes');
    }
}
