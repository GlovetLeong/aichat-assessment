<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerCampaignVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_campaign_vouchers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->bigInteger('campaign_voucher_id')->unsigned()->nullable();
            $table->foreign('campaign_voucher_id')->references('id')->on('campaign_vouchers')->onDelete('cascade');
            $table->string('name');
            $table->string('code');
            $table->integer('discount_type')->nullable()->default(1);
            $table->float('discount_amount', 20, 4)->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();

            $table->index(['code', 'discount_amount', 'start_date', 'end_date', 'created_at'], 'ccv_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_campaign_vouchers');
    }
}
