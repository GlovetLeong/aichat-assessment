<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRedeemCampaignProcessTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_redeem_campaign_process_transaction', function (Blueprint $table) {
            $table->bigInteger('customer_redeem_campaign_process_id')->unsigned()->nullable();
            $table->foreign('customer_redeem_campaign_process_id', 'crcpt_crcp_id')->references('id')->on('customer_redeem_campaign_processes')->onDelete('cascade');
            $table->bigInteger('customer_purchase_transaction_id')->unsigned()->nullable();
            $table->foreign('customer_purchase_transaction_id', 'crcpt_cpt_id')->references('id')->on('customer_purchase_transactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_redeem_campaign_process_transaction');
    }
}
