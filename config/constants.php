<?php

return [
    'status' => [
        'active' => 1,
        'deactive' => 2,
        'delete' => 3,
        'clear' => 4,
        'pending' => 9,
        'reject_login' => 14,
    ],
    'gender' => [
        'male' => 1,
        'female' => 2,
    ],
    'discount_type' => [
        'flat' => 1,
        'percentage' => 2,
    ]
];
