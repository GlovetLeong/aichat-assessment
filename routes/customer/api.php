<?php

Route::group(['namespace' => 'Customer', 'prefix' => 'customer-backsys/api'], function () {
	Route::group(['namespace' => 'Api'], function () {
    	Route::post('login', ['as' => 'api.customer.login', 'uses' => 'LoginController@login']);
		// Customer
		Route::prefix('customer')->group(function () {
			Route::get('self', 
	    		[
					'as' => 'api.customer.self', 
					'uses' => 'UserController@self', 
	    		]
	    	);
		});

		Route::prefix('campaign')->group(function () {
			Route::get('list', 
	    		[
					'as' => 'api.customer.campaign.list', 
					'uses' => 'CampaignController@list', 
	    		]
	    	);
		});

		Route::prefix('custome-redeem-campaign-processes')->group(function () {
			Route::post('store', 
	    		[
					'as' => 'api.customer.custome-_redeem-campaign-processes.store', 
					'uses' => 'CustomerRedeemCampaingProcessController@store', 
	    		]
	    	);
	    	Route::put('upload-img-and-complete/{hash_id}', 
	    		[
					'as' => 'api.customer.custome-_redeem-campaign-processes.upload-img-and-complete', 
					'uses' => 'CustomerRedeemCampaingProcessController@uploadImgAndComplete', 
	    		]
	    	);
		});
	});
});