<?php

use Illuminate\Support\Facades\Route;

Route::get('unauthorized', ['as' => 'unauthorized', function() {
    return response('Unauthorized Access', 401);
}]);
