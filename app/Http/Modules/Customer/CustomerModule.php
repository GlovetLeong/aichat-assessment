<?php

namespace App\Http\Modules\Customer;

use Auth;
use Illuminate\Http\Request;

class CustomerModule
{
    public function __construct()
    {
        // parent::__construct();
    }

    public static function self(Request $request)
    {
    	$self = Auth::guard('customer-api')->user();

    	$data = (object)[
            'status' => true,
            'data' => $self
        ];

        return response()->json($data);
    }
}
