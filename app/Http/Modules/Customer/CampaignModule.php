<?php

namespace App\Http\Modules\Customer;

use Auth;
use App\Models\Campaign;

use App\Http\Helpers\General;

use Illuminate\Http\Request;

class CampaignModule
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function list(Request $request)
    {
        General::storeCachePerviousQuery($request->query(), 'campaign_query');
        
        $status = $request->get('status', []);
        $page = $request->get('page', null);
        $limit = $request->get('limit', 10);

        $query = Campaign::query();

        if (!empty($status) > 0) {
            $query->whereIn('status', $status);
        }
        else {
            $query->whereNotIn('status', 
                [
                    config('constants.status.delete')
                ]
            );
        }

        $query->orderBy('id', 'desc');

        $params = [
            'status' => $status,
            'page' => $page
        ];

        $campaign_list = $query->paginate($limit);

        $extract = [];
        foreach ($campaign_list as $key => $row) {
            $extract[] = $row;

        }
        $pagination = General::paginationGenerator($campaign_list, $params, $extract);

        return $pagination;
    }
}
