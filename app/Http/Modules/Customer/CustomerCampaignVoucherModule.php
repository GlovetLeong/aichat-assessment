<?php

namespace App\Http\Modules\Customer;

use Auth;
use App\Models\CustomerCampaignVoucher;
use App\Models\CampaignVoucher;

use App\Http\Rules\Customer\ExistCheck;
use App\Http\Rules\Customer\StatusCheck;

use App\Http\Helpers\General;
use App\Http\Helpers\Hasher;
use App\Http\Helpers\ImageHandel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerCampaignVoucherModule
{
    public function __construct()
    {
        
    }

    public static function store(Request $request)
    {
        $validation = CustomerCampaignVoucherModule::validation($request, '', 'POST');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $campaign_voucher_id = $request->input('campaign_voucher_id');
        $campaign_voucher_id = Hasher::decode('campaign_vouchers', $campaign_voucher_id[0]);

        $campaign_voucher = CampaignVoucher::find($campaign_voucher_id);

        $customer_campaign_voucher = new CustomerCampaignVoucher();
        $customer_campaign_voucher->customer_id = Auth::guard('customer-api')->user()->id;
        $customer_campaign_voucher->campaign_voucher_id = $campaign_voucher->id;
        $customer_campaign_voucher->name = $campaign_voucher->name;
        $customer_campaign_voucher->code = $campaign_voucher->code;
        $customer_campaign_voucher->discount_type = $campaign_voucher->discount_type;
        $customer_campaign_voucher->discount_amount = $campaign_voucher->discount_amount;
        $customer_campaign_voucher->status = config('constants.status.active');
        $customer_campaign_voucher->start_date = $campaign_voucher->start_date;
        $customer_campaign_voucher->end_date = $campaign_voucher->end_date;
        $customer_campaign_voucher->save();

        DB::commit();

        $data = General::returnData($customer_campaign_voucher);

        return response()->json($data);
    }

    private static function validation(Request $request, $id = '', $method = 'POST')
    {
        $data = $request->all();

        $rule= [];
        if ($method == 'POST') {
            $rule = [
                'campaign_voucher_id' => [
                    'required', 
                    new ExistCheck('campaign_voucher'),
                    new StatusCheck('campaign_voucher',
                        [
                            config('constants.status.delete')
                        ]
                    ),
                ],
            ];
        }

        $validator = Validator::make($data, $rule, config('error_code'));

        $errors = $validator->errors();

        if ($validator->fails()) {
            $data = (object)[
                'status' => false,
                'errors' => $errors
            ];
            return $data;
        }
        else {
            return (object)['status' => true];
        }
    } 
}
