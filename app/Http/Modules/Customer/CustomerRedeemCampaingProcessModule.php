<?php

namespace App\Http\Modules\Customer;

use Auth;
use App\Models\CustomerRedeemCampaignProcess;
use App\Models\CampaignVoucher;
use App\Models\CustomerPurchaseTransaction;

use App\Http\Modules\Customer\CustomerCampaignVoucherModule;

use App\Http\Rules\Customer\ExistCheck;
use App\Http\Rules\Customer\StatusCheck;
use App\Http\Rules\Customer\UnderCheck;
use App\Http\Rules\Customer\CustomerRedeemCampaignCheck;
use App\Http\Rules\Customer\CampaignEnterCheck;
use App\Http\Rules\Customer\Image64Check;

use App\Http\Helpers\General;
use App\Http\Helpers\Hasher;
use App\Http\Helpers\ImageHandel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CustomerRedeemCampaingProcessModule
{
    public function __construct()
    {
        
    }

    public static function store(Request $request)
    {
        $validation = CustomerRedeemCampaingProcessModule::validation($request, '', 'POST');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $campaign_id = $request->input('campaign_id');
        $campaign_id = Hasher::decode('campaigns', $campaign_id[0]);

        $campaign_voucher = CampaignVoucher::whereDoesntHave('customer_campaign_voucher', function($query) {
            $query->whereIn('status', [
                config('constants.status.active'),
                config('constants.status.pending')
            ]);
        })
        ->where('status', config('constants.status.active'))
        ->sharedLock()
        ->first();

        $customer_redeem_campaign_process = new CustomerRedeemCampaignProcess();
        $customer_redeem_campaign_process->campaign_id = $campaign_id;
        $customer_redeem_campaign_process->customer_id = Auth::guard('customer-api')->user()->id;
        $customer_redeem_campaign_process->campaign_voucher_id = $campaign_voucher->id;
        $customer_redeem_campaign_process->status = config('constants.status.pending');
        $customer_redeem_campaign_process->save();

        DB::commit();

        $data = General::returnData($customer_redeem_campaign_process);

        return response()->json($data);
    }

    public static function uploadImgAndComplete(Request $request, $hash_id)
    {
        $id = Hasher::decode('customer_redeem_campaign_processes', $hash_id);
        $validation = CustomerRedeemCampaingProcessModule::validation($request, $id, 'UPLOAD_COMPLETE');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $customer_redeem_campaign_process = CustomerRedeemCampaignProcess::find($id);

        // img
        $img = $request->input('img');
        if (!empty($img)) {
            $img = json_encode(ImageHandel::insertImage('customer_redeem_campaign_process', $img));
            $customer_redeem_campaign_process->img = $img;
            if (!$img) {
                DB::rollBack();
            }
        }

        $customer_redeem_campaign_process->status = config('constants.status.active');
        $customer_redeem_campaign_process->update();

        $request->request->add([
            'campaign_voucher_id' => [$customer_redeem_campaign_process->campaign_voucher_hash_id],
        ]);

        $customer_campaign_voucher = CustomerCampaignVoucherModule::store($request);


        $last_30_day = Carbon::now()->subDays(30)->format('Y-m-d');

        $customer_purchase_transaction_id = CustomerPurchaseTransaction::
            where('customer_id', Auth::guard('customer-api')->user()->id)
            ->where('created_at', '>=', $last_30_day)
            ->orderBy('total_spent', 'desc')
            ->limit(3)
            ->pluck('id');

        $customer_redeem_campaign_process->customer_purchase_transaction()->sync($customer_purchase_transaction_id);

        DB::commit();

        $data = General::returnData($customer_redeem_campaign_process);

        return response()->json($data);
    }

    public static function delete(Request $request, $hash_id)
    {
        $id = Hasher::decode('customer_redeem_campaign_processes', $hash_id);

        DB::beginTransaction();

        $customer_redeem_campaign_process = CustomerRedeemCampaignProcess::find($id);
        $customer_redeem_campaign_process->status = config('constants.status.delete');
        $customer_redeem_campaign_process->update();

        DB::commit();

        $data = General::returnData($customer_redeem_campaign_process);

        return response()->json($data);
    }

    private static function validation(Request $request, $id = '', $method = 'POST')
    {
        $data = $request->all();

        $rule= [];
        if ($method == 'POST') {
            $rule = [
                'campaign_id' => [
                    'required', 
                    new ExistCheck('campaign'),
                    new CampaignEnterCheck()
                ],
            ];
        }

        if ($method == 'UPLOAD_COMPLETE') {
            $data['id'] = $id;
            $rule = [
                'id' => [
                    new ExistCheck('customer_redeem_campaign_process'), 
                    new UnderCheck('customer_redeem_campaign_process'),
                    new StatusCheck('customer_redeem_campaign_process',
                        [
                            config('constants.status.active'),
                            config('constants.status.delete')
                        ]
                    ),
                    new CustomerRedeemCampaignCheck()
                ],
                'img' => 'required'
            ];

        }


        if (!empty($data['img'])) {
            foreach ($data['img'] as $key => $row) {
                $rule['img'.'.'.$key] = ['required', new Image64Check()];
            }
        }

        $validator = Validator::make($data, $rule, config('error_code'));

        $errors = $validator->errors();

        if (!empty($data['img'])) {
            foreach ($data['img'] as $key => $row) {
                if ($errors->first('img'.'.'.$key)) {
                    $errors->add('img', config('error_code.img_error'));
                    break;
                }
            }
        }

        if ($validator->fails()) {
            $data = (object)[
                'status' => false,
                'errors' => $errors
            ];
            return $data;
        }
        else {
            return (object)['status' => true];
        }
    } 
}
