<?php

namespace App\Http\Rules\Customer;

use Auth;
use App\Models\CustomerRedeemCampaignProcess;

use App\Http\Modules\Customer\CustomerRedeemCampaingProcessModule;

use App\Http\Helpers\Hasher;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Contracts\Validation\Rule;

class CustomerRedeemCampaignCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $value = is_array($value) ? $value : [$value];
        $customer_redeem_campaign_process_id = Hasher::decode('customer_redeem_campaign_processes', $value[0]);

        $customer_redeem_campaign_process = CustomerRedeemCampaignProcess::find($customer_redeem_campaign_process_id);

        // check for exceeds10 min
        $afetr_10_min = Carbon::now()->timezone('Asia/Kuala_Lumpur')->subMinutes(10)->format('His');
        $customer_redeem_campaign_process_created_at = Carbon::parse($customer_redeem_campaign_process->created_at)->format('His');
        
        if ($customer_redeem_campaign_process_created_at < $afetr_10_min) {
            $request = new Request();
            $request->setMethod('PUT');
            CustomerRedeemCampaingProcessModule::delete($request, $customer_redeem_campaign_process->hash_id);
            return false;
        }

        // image recognition
        $image_recognition = true;
        if (!$image_recognition) {
            // faking this process for now
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return config('error_code.redeem_expired');
    }
}
