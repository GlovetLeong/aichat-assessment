<?php

namespace App\Http\Rules\Customer;

use Auth;
use App\Models\CustomerRedeemCampaignProcess;

use App\Http\Helpers\Hasher;

use Illuminate\Contracts\Validation\Rule;

class UnderCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $type;

    public function __construct($type = '')
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $value = is_array($value) ? $value : [$value];

        if ($this->type == 'customer_redeem_campaign_process') {

            foreach ($value as $key => $row) {
                if(!empty($row)) {
                    $id = Hasher::decode('customer_redeem_campaign_processes', $row);
                    $data = CustomerRedeemCampaignProcess::
                        where('id', $id)
                        ->where('customer_id', Auth::guard('customer-api')->user()->id)
                        ->count();
                    if (!$data) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return config('error_code.under_error');
    }
}
