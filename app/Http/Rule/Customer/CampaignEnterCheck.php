<?php

namespace App\Http\Rules\Customer;

use Auth;
use App\Models\Campaign;
use App\Models\CustomerRedeemCampaignProcess;
use App\Models\CustomerPurchaseTransaction;

use App\Http\Helpers\Hasher;
use Carbon\Carbon;

use Illuminate\Contracts\Validation\Rule;

class CampaignEnterCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $error_code;

    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $value = is_array($value) ? $value : [$value];
        $campaign_id = Hasher::decode('campaigns', $value[0]);

        // check campaign still valid to enter
        $campaign = Campaign::find($campaign_id);
        $now = Carbon::now()->format('YmdHis');
        $campaign_end_date = Carbon::parse($campaign->end_date)->format('YmdHis');

        if ($campaign_end_date < $now) {
            $this->error_code = config('error_code.campaign_expired');
            return false;
        }

        // check this customer already redeem or enter
        $customer_redeem_campaign_process_exist = CustomerRedeemCampaignProcess::
            where('customer_id', Auth::guard('customer-api')->user()->id)
            ->where('campaign_id', $campaign_id)
            ->whereIn('status', [
                config('constants.status.active'),
                config('constants.status.pending')
            ])
            ->count();

        if ($customer_redeem_campaign_process_exist) {
            $this->error_code = config('error_code.campaign_already_enter');
            return false;
        }

        // check customer transaction last 30 days
        $last_30_day = Carbon::now()->subDays(30)->format('Y-m-d');

        $customer_purchase_transaction = CustomerPurchaseTransaction::
            where('customer_id', Auth::guard('customer-api')->user()->id)
            ->where('created_at', '>=', $last_30_day)
            ->get();

        if (count($customer_purchase_transaction) < 3) {
            $this->error_code = config('error_code.campaign_transaction_not_more_than_3');
            return false;
        }

        $total_spent = 0;
        foreach ($customer_purchase_transaction as $key => $row) {
            $total_spent += $row->total_spent;
        }

        if ($total_spent < 100) {
            $this->error_code = config('error_code.campaign_transaction_spent_not_more_than_100');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->error_code;
    }
}
