<?php

namespace App\Http\Rules\Customer;

use Auth;
use App\Models\Campaign;
use App\Models\CustomerRedeemCampaignProcess;
use App\Models\CampaignVoucher;

use App\Http\Helpers\Hasher;

use Illuminate\Contracts\Validation\Rule;

class ExistCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $type;

    public function __construct($type = '')
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $value = is_array($value) ? $value : [$value];

        if ($this->type == 'campaign') {
            foreach ($value as $key => $row) {
                if(!empty($row)) {
                    $id = Hasher::decode('campaigns', $row);
                    $data = Campaign::find($id);
                    if (!$data) {
                        return false;
                    }
                }
            }
        }

        if ($this->type == 'customer_redeem_campaign_process') {
            foreach ($value as $key => $row) {
                if(!empty($row)) {
                    $id = Hasher::decode('customer_redeem_campaign_processes', $row);
                    $data = CustomerRedeemCampaignProcess::find($id);
                    if (!$data) {
                        return false;
                    }
                }
            }
        }

        if ($this->type == 'campaign_voucher') {
            foreach ($value as $key => $row) {
                if(!empty($row)) {
                    $id = Hasher::decode('campaign_vouchers', $row);
                    $data = CampaignVoucher::find($id);
                    if (!$data) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return config('error_code.exist_error');
    }
}
