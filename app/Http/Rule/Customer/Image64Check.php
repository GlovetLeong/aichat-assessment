<?php

namespace App\Http\Rules\Customer;

use Illuminate\Contracts\Validation\Rule;

class Image64Check implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        
        $accept_mime_type = ['image/gif', 'image/png', 'image/jpeg'];

        $f = finfo_open();
        $mime_type = finfo_file($f, $value, FILEINFO_MIME_TYPE);

        if (!in_array($mime_type, $accept_mime_type)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return config('error_code.img_error');
    }
}
