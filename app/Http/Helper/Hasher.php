<?php
namespace App\Http\Helpers;

use Hashids\Hashids;

class Hasher
{
    public function __construct()
    {

    }

    public static function encode($entity = '', ...$args)
    {
        $hash = new Hashids($entity, 20);
        return $hash->encode(...$args);
    }

    public static function decode($entity = '', $enc)
    {
        if (is_int($enc)) {
            return $enc;
        }

        $hash = new Hashids($entity, 20);
        return $hash->decode($enc) ? $hash->decode($enc)[0] : '';
    }

     public static function decodeArray($entity = '', $set = [])
    {
        $return_set = [];
        foreach ($set as $key => $row) {
            $return_set[] = Hasher::decode($entity, $row);
        }

        return $return_set;
    }
}

