<?php
namespace App\Http\Helpers;

use Illuminate\Support\Str;
use Image;

class ImageHandel
{
	function __construct()
	{

	}

	public static function getImgFullPath($img_info = [], $type = 'object')
	{
		$default_file =  asset('share/no-image.png');
		
		$img_info = json_decode($img_info);
		if (!empty($img_info)) {
			foreach ($img_info as $key_img => $row_img) {
				$img_info[$key_img]->file_info->original->file = $row_img->path != 'seeder' ? 
					asset('storage/' . $row_img->file_info->original->file) :
					asset($row_img->file_info->original->file);
			}
		}
		else {
			if ($type == 'object') {
				$default_file =  asset('share/non-image.png');
			}
			else if ($type == 'person') {
				$default_file =  asset('share/non-person.png');
			}
			else if ($type == 'logo') {
				$default_file =  asset('share/non-logo.png');
			}
			else if ($type == 'default_profile') {
				$default_file =  asset('share/img/default_profile.png');
			}

			$img_info = [(object)[
				'empty' => true,
				'file_info' => (object)[
						'original' => (object) [
							'file' => $default_file
						],
				]
			]];
		}
	    return $img_info;
	}

	public static function extractFullPath($file = '')
	{
		$file = explode(asset('/storage'), $file);
		return $file[1];
	}

	public static function generateFolder($path = '')
    {
        $folder = \Storage::disk('public')->makeDirectory($path);
        return $folder;
    }

    public static function insertImage($path = '', $file = '')
    {
        $image = ImageHandel::base64ToImage($path, $file);
        return $image;
    }

    public static function base64ToImage($path = '', $base64Set = [])
    {
    	$img_info = [];
        $folder_path = ImageHandel::generateFolder('images/' . $path);

    	foreach ($base64Set as $key => $row) {

	    	$imgdata = $row;
	        $filename = (string) Str::uuid();

	        $f = finfo_open();
	        $mime_type = finfo_file($f, $imgdata, FILEINFO_MIME_TYPE);
	        
	        $extension = '';
	        switch ($mime_type) {
	            case 'image/gif':
	                $extension = 'gif';
	                break;
	            case 'image/png':
	                $extension = 'png';
	                break;
	            case 'image/jpeg':
	                $extension = 'jpg';
	                break;
	        }

	        if (empty($extension)) {
	            return false;
	        }

	        $file_info = [];

	        // $filename = $filename . '.' . $extension;
	        $filename = $filename . '.jpeg';
	        $img = Image::make($row)->save(storage_path('app/public/images/' . $path) . '/' . $filename);

			$file_info['original']['file'] = 'images/' . $path . '/' . $filename;
			$file_info['original']['width'] = $img->width();
			$file_info['original']['height'] = $img->height();

	        $img_info[] = [
	        	'on_cloud' => false,
	        	'size' => ImageHandel::getBase64Size($row),
	            'path' => $path,
	            'filename' => $filename,
	            'mime_type' => $mime_type,
	            'extension' => $extension,
	            'file_info' => empty($file_info) ? [] : $file_info,
	        ];
    	}

        return $img_info;
    }

    public static function removeImage($img_info = [])
    {
        foreach ($img_info as $key_img => $row_img) {
        	if (!empty($row_img->path) && $row_img->path != 'seeder') {
	    		$img_original = ImageHandel::extractFullPath($row_img->file_info->original->file);
	            \Storage::disk('public')->delete([$img_original]);
           }
        }
        return true;
    }

    public static function getBase64Size($base64)
    {
	    try{
	    	$return_size = 0;
	        $size_in_bytes = (int) (strlen(rtrim($base64, '=')) * 3 / 4);
	        if ($size_in_bytes >= 1 && $size_in_bytes >= 1024) {
	        	$size_in_kb = $size_in_bytes / 1024;
	        	if ($size_in_kb >= 1 && $size_in_kb >= 1024) {
	        		$size_in_mb = $size_in_kb / 1024;
	        		$return_size = round($size_in_mb) . ' mb';
	        	}
	        	else {
	        		$return_size = round($size_in_kb) . ' kb';
	        	}
	        }
	        else {
	        	$return_size = round($size_in_bytes) . ' bytes';
	        }

	        return $return_size;
	    }
	    catch(Exception $e){
	        return $e;
	    }
	}
}