<?php
namespace App\Http\Helpers;

use Illuminate\Support\Facades\Cache;

class General
{
    public function __construct()
    {

    }

    public static function returnTokenData($object = [], $token = '')
    {
        $data = (object)[
            'status' => true,
            'access_token' => $token,
            'data' => $object
        ];

        return $data;
    }
    
    public static function returnErrorData($error = [])
    {
        $data = (object)[
            'status' => false,
            'errors' => $error
        ];

        return $data;
    }

    public static function returnData($object = [], $query_string = '')
    {
        if ($query_string) {
            $data = (object)[
                'status' => true,
                'pervious_query' => $query_string,
                'data' => $object
            ];
        }
        else {
            $data = (object)[
                'status' => true,
                'data' => $object
            ];
        }

        return $data;
    }

    public static function storeCachePerviousQuery($query = '', $name = '')
    {   
        $query_string = urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', http_build_query($query)));
        Cache::put($name, $query_string, 1800);
    }

    public static function getCachePerviousQuery($name = '')
    {   
        return Cache::get($name);
    }

    public static function paginationGenerator($data = [], $params = [], $extract = [])
    {   
        $pagination = [];

        $data->appends($params)->links();
        $pagination = [
            'status' => true,
            'total' => $data->total(),
            'has_more_page' => $data->hasMorePages(),
            'query_string' => urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', http_build_query($params))),
            'page' => [
                'next_page' =>  urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', $data->nextPageUrl())),
                'pervious_page' =>  urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', $data->previousPageUrl())),
            ],
            'data' => $extract
        ];

        return $pagination;
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

