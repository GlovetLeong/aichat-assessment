<?php

namespace App\Http\Controllers\Customer\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\Customer\LoginModule;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
    	
    }

    public function login(Request $request)
    {
    	$login = LoginModule::login($request);
    	return $login;
    }
}
