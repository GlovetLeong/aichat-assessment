<?php

namespace App\Http\Controllers\Customer\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\Customer\CustomerRedeemCampaingProcessModule;
	
use App\Http\Controllers\Customer\BaseController;

class CustomerRedeemCampaingProcessController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(Request $request)
    {
    	$customer_redeem_campaign_process = CustomerRedeemCampaingProcessModule::store($request);
    	return $customer_redeem_campaign_process;
    }

    public function uploadImgAndComplete(Request $request, $hash_id)
    {
    	$customer_redeem_campaign_process = CustomerRedeemCampaingProcessModule::uploadImgAndComplete($request, $hash_id);
    	return $customer_redeem_campaign_process;
    }
}
