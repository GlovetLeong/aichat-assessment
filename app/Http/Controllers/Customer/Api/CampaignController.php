<?php

namespace App\Http\Controllers\Customer\Api;

use Illuminate\Http\Request;

use App\Http\Modules\Customer\CampaignModule;

use App\Http\Controllers\Customer\BaseController;

class CampaignController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list(Request $request)
    {
    	$campaign = CampaignModule::list($request);
    	return $campaign;
    }
}
