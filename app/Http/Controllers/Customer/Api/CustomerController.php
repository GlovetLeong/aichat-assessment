<?php

namespace App\Http\Controllers\Customer\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\Customer\CustomerModule;

use App\Http\Controllers\Customer\BaseController;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function self(Request $request)
    {
    	$self = CustomerModule::self($request);
    	return $self;
    }
}
