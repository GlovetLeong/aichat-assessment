<?php

namespace App\Models;

use App\Http\Helpers\Hasher;
use App\Http\Helpers\ImageHandel;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CustomerRedeemCampaignProcess extends Model
{
    protected $table = 'customer_redeem_campaign_processes';

    protected $appends = ['hash_id', 'campaign_voucher_hash_id', 'campaign_list', 'customer_list', 'campaign_voucher_list'];

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'campaign_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function campaign_voucher()
    {
        return $this->belongsTo('App\Models\CampaignVoucher', 'campaign_voucher_id');
    }

    public function customer_purchase_transaction()
    {
        return $this->belongsToMany('App\Models\CustomerPurchaseTransaction', 'customer_redeem_campaign_process_transaction');
    }

    public function getHashIdAttribute()
    {
        return Hasher::encode('customer_redeem_campaign_processes', $this->id);
    }

    public function getCampaignVoucherHashIdAttribute()
    {
        return Hasher::encode('campaign_vouchers', $this->campaign_voucher_id);
    }

    public function getCampaignListAttribute()
    {
        return $this->campaign()->get();
    }

    public function getCustomerListAttribute()
    {
        return $this->customer()->get();
    }

    public function getCampaignVoucherListAttribute()
    {
        return $this->campaign_voucher()->get();
    }

    public function getImgAttribute($value)
    {
        $file_info = [];
        $file_info = ImageHandel::getImgFullPath($value, 'object');
        return $file_info;
    }

    public function getUpdatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }

    public function getCreatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }
}
