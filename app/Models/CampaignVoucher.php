<?php

namespace App\Models;

use App\Http\Helpers\Hasher;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CampaignVoucher extends Model
{
    protected $table = 'campaign_vouchers';

    protected $appends = ['hash_id'];

	public function customer_redeem_campaign_process()
    {
        return $this->hasMany('App\Models\CustomerRedeemCampaignProcess', 'campaign_voucher_id');
    }

    public function customer_campaign_voucher()
    {
        return $this->hasMany('App\Models\CustomerCampaignVoucher', 'campaign_voucher_id');
    }

    public function getHashIdAttribute()
    {
        return Hasher::encode('campaign_vouchers', $this->id);
    }

    public function getUpdatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }

    public function getCreatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }
}
