<?php

namespace App\Models;

use App\Http\Helpers\Hasher;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Campaign extends Model
{
    protected $table = 'campaigns';

    protected $appends = ['hash_id', 'company_list'];

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function getHashIdAttribute()
    {
        return Hasher::encode('campaigns', $this->id);
    }

    public function getCompanyListAttribute()
    {
        return $this->company()->get();
    }

    public function getUpdatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }

    public function getCreatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }
}
