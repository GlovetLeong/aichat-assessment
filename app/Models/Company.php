<?php

namespace App\Models;

use App\Http\Helpers\Hasher;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Company extends Model
{
    protected $table = 'companies';

    protected $appends = ['hash_id'];

    public function getHashIdAttribute()
    {
        return Hasher::encode('companies', $this->id);
    }

    public function getUpdatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }

    public function getCreatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }
}
