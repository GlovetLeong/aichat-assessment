<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerRedeemCampaignProcessTransaction extends Model
{
    use HasFactory;
}
